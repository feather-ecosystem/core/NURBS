# NURBS

This package implements Non-Uniform Rational B-splines mappings, their derivatives, refinement and some primitive geometries. Further reading:

1. De Boor, Carl, et al. *A practical guide to splines*. Vol. 27. New York: springer-verlag, 1978.
2. Piegl, Les, and Wayne Tiller. *The NURBS book*. Springer Science & Business Media, 2012.
3. Schumaker, Larry. *Spline functions: basic theory*. Cambridge University Press, 2007.
4. Rogers, David F. *An introduction to NURBS: with historical perspective*. Elsevier, 2000.
