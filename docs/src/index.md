# NURBS.jl

[![pipeline status](https://gitlab.com/feather-ecosystem/NURBS/badges/master/pipeline.svg)](https://gitlab.com/feather-ecosystem/NURBS/-/commits/master)
[![coverage report](https://gitlab.com/feather-ecosystem/NURBS/badges/master/coverage.svg)](https://gitlab.com/feather-ecosystem/NURBS/-/commits/master)
[![codecov](https://codecov.io/gl/feather-ecosystem:core/NURBS/branch/master/graph/badge.svg?token=Z1PP61HV1G)](https://codecov.io/gl/feather-ecosystem:core/NURBS)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://feather-ecosystem.gitlab.io/core/NURBS)

***

This package implements Non-Uniform Rational B-splines mappings, their derivatives, refinement and some primitive geometries. Further reading:

1. De Boor, Carl, et al. *A practical guide to splines*. Vol. 27. New York: springer-verlag, 1978.
2. Piegl, Les, and Wayne Tiller. *The NURBS book*. Springer Science & Business Media, 2012.
3. Schumaker, Larry. *Spline functions: basic theory*. Cambridge University Press, 2007.
4. Rogers, David F. *An introduction to NURBS: with historical perspective*. Elsevier, 2000.

!!! tip
    This package is a part of the [`Feather`](https://gitlab.com/feather-ecosystem) project. It is tightly integrated into the ecosystem of packages provided by Feather. If you are interested in applications of the functionality implemented in this package, please visit the main documentation of the [`ecosystem`](https://feather-ecosystem.gitlab.io/feather/)