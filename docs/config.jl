using NURBS 

package_info = Dict(
    "modules" => [NURBS],
    "authors" => "Rene Hiemstra and contributors",
    "name" => "NURBS.jl",
    "repo" => "https://gitlab.com/feather-ecosystem/core/NURBS",
    "pages" => [
        "About"  =>  "index.md"
        "API"  =>  "api.md"
    ],
)

DocMeta.setdocmeta!(NURBS, :DocTestSetup, :(using NURBS); recursive=true)

# if docs are built for deployment, fix the doctests
# which fail due to round off errors...
if haskey(ENV, "DOC_TEST_DEPLOY") && ENV["DOC_TEST_DEPLOY"] == "yes"
    doctest(NURBS, fix=true)
end
